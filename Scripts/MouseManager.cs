﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.EventSystems;

public class MouseManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    public LayerMask hitMask;
    public float MaxDist;

    EventSystem _ES;
    EventSystem ES { get {
            if (_ES == null)
                _ES = GameObject.FindObjectOfType<EventSystem>();
            return _ES;
        } }

    GameObject LastObject;
	// Update is called once per frame
	void Update () {
        Vector2 screenPos = Input.mousePosition;
        Ray CamRay = Camera.main.ScreenPointToRay(screenPos);

        if (ES.IsPointerOverGameObject()) {

        }
        else {
            RaycastHit hitInfo;
            if (Physics.Raycast(CamRay, out hitInfo, MaxDist, hitMask)) {
                SetTarget(hitInfo.collider.gameObject);
            }
            else {
                SetTarget(null);
            }

            if (Input.GetMouseButtonUp(0)) {
                if (LastObject != null) {
                    Clickable C;
                    Debug.Log("MouseUp");
                    if ((C = LastObject.GetComponentInParent<Clickable>()) != null) {
                        Debug.Log("Target = "+C);
                        C.OnClicked();
                    }
                }
            }
        }
	}

    

    void SetTarget(GameObject newTarget) {
        if (LastObject == newTarget) return;
        Hoverable Old = LastObject==null ? null : LastObject .GetComponentInParent<Hoverable>();
        Hoverable New = newTarget ==null ? null : newTarget.GetComponentInParent<Hoverable>();
        LastObject = newTarget;

        
        if (Old!= New) {
            if (Old != null) Old.OnExit();
            if (New != null) New.OnEnter();
        }
    }


    public interface Clickable {
        void OnClicked();
    }

    public interface Hoverable {
        void OnEnter();
        void OnExit();
    }
}
