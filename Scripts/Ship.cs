﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Ship : MonoBehaviour, MouseManager.Clickable, MouseManager.Hoverable {

    public int TransferCost;
    public string ShipName;
    public int MilitaryStr;
    


    public bool isCivilian;

    bool _isTransferred;
    public bool isTransferred {
        get {
            return _isTransferred;
        }
        set {
            if (_isTransferred != value) {
                _isTransferred = value;
                //Vector3 curOffset = transform.localPosition;
                float WarpDur = SoundManager.Main.PlayWarp();
                float F1 = WarpDur / 5;
                float F2 = WarpDur - F1;
                GameManager.Main.ShipRepositioned(this, value);
                UpdateSelection();

                AnimationSynchronized.Main.StartAnimation(0, F1, OpenWarp, () => {
                    transform.localPosition = localOffset ;
                    AnimationSynchronized.Main.StartAnimation(0, F2, CloseWarp, null);
                });
            }
        }
    }

    void OpenWarp(float pct) {
        foreach (WarpEffect WE in GetComponentsInChildren<WarpEffect>()) {
            WE.OpenWarp(pct);
        }
    }
    void CloseWarp(float pct) {
        foreach (WarpEffect WE in GetComponentsInChildren<WarpEffect>()) {
            WE.CloseWarp(pct);
        }
    }

    public bool isHovered { get; private set; }

    public bool IsTransfering {
        get {
            return GameManager.Main.WillShipMove(this);
        }
    }

    Action _OnClicked;

    public void OnClicked() {
        Debug.Log("Blam!");
        //_OnClicked();

        if (IsTransfering) GameManager.Main.CanCancel(this);
        else GameManager.Main.TryTransfer(this);
        
        UpdateSelection();
    }

    //Vector3 startPos;
    //Vector3 dest;
    //public float Vel;
    //public float Maneuver;
    public Vector3 localOffset;

    //Quaternion startRot;
    //Quaternion ReqRot;

    //public void NavigateTo(Vector3 WorldPos, System.Action OnComplete) {
    //    RotateToPoint(WorldPos, () => {
    //        MoveToPoint(WorldPos, OnComplete);
    //    });
    //}

    //void RotateToPoint(Vector3 WorldPos, System.Action OnComplete) {

    //    startRot = transform.rotation;
    //    Debug.Log("Direction to look: " + (WorldPos - transform.position));
    //    ReqRot = Quaternion.FromToRotation(Vector3.right, WorldPos - transform.position);
    //    Debug.Log("Need to look at: " + ReqRot.eulerAngles);
    //    AnimationSynchronized.Main.StartAnimation(0, Quaternion.Angle(startRot, ReqRot) / Maneuver, ProgressRot, OnComplete);
    //}

    //void ProgressRot(float pct) {
    //    transform.rotation = Quaternion.Lerp(startRot, ReqRot, pct);
    //}

    //void MoveToPoint(Vector3 WorldPoint, System.Action OnComplete) {
    //    startPos = transform.position;
    //    dest = WorldPoint;
    //    AnimationSynchronized.Main.StartAnimation(0, Vector3.Distance(startPos, dest) / Vel, ProgressToPoint, OnComplete);
    //}



    //void ProgressToPoint(float pct) {
    //    transform.position = Vector3.Lerp(startPos, dest, pct);
    //}

    //void InitTransfer() {

    //    if (isCivilian) {

    //    }
    //    else {

    //    }

    //    Debug.Log("Init Transfer of " + gameObject);

    //    IsTransfering = true;
    //    SelectionManager.Main.ShowSelection(gameObject, Color.green);

    //    NavigateTo(Vector3.zero, CancelTransfer);

    //    _OnClicked = CancelTransfer;
    //}

    //void CancelTransfer() {
    //    IsTransfering = false;
    //    OnEnter();
    //    _OnClicked = InitTransfer;
    //}


    // Use this for initialization
    void Start () {
        localOffset = transform.localPosition;
        //_OnClicked = InitTransfer;
        CloseWarp(1);
    }
	
	// Update is called once per frame
	void Update () {
		
	}


    bool wasTransferring, wasHovering;

    void UpdateSelection() {
        if (IsTransfering) {
            if (!wasTransferring) {
                wasTransferring = true;
                SoundManager.Main.PlaySelect();
                SelectionManager.Main.ShowSelection(gameObject, Color.green);
            }
        }
        else {
            if (wasTransferring) {
                //SoundManager.Main.PlaySelect();
                wasTransferring = false;
            }
            if (isHovered)
                SelectionManager.Main.ShowSelection(gameObject, Color.yellow);
            else
                SelectionManager.Main.StopSelection(gameObject);
        }

    }

    public void OnEnter() {
        isHovered = true;
        UpdateSelection();
        //if (IsTransfering) return; 
        //Debug.Log("Hower over ship");
        //SelectionManager.Main.ShowSelection(gameObject, Color.yellow);
    }

    public void OnExit() {
        isHovered = false;
        UpdateSelection();
        //if (IsTransfering) return;
        //SelectionManager.Main.StopSelection(gameObject);
    }
}
