﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
[RequireComponent(typeof(RectTransform))]
public class Selection : MonoBehaviour {
    public float padding;
    Image _myIMG;
    Image myImage { get {
            if (_myIMG==null) {
                _myIMG = GetComponent<Image>();
            }
            return _myIMG;
        } }

    RectTransform _myRT;
    RectTransform myRT { get {
            if (_myRT == null) {
                _myRT = GetComponent<RectTransform>();
            }
            return _myRT;
        } }

    public event System.Action<Selection> OnInactive;

    public void ShowSelection(GameObject target, Color Color) {
        Vector2 Pad = new Vector2(padding, padding);

        Renderer Ren = target.GetComponent<Renderer>();

        Rect targetRect;
        if (Ren != null) {
            targetRect = WorldToScreenBounds(Ren.bounds);
        }
        else {
            targetRect = DefaultRect(target.transform.position, 20);
        }

        myRT.anchoredPosition = targetRect.position - Pad;
        myRT.sizeDelta = targetRect.size+ Pad*2;
        myRT.hasChanged = true;

        myImage.color = Color;

        gameObject.SetActive(true);
    }

    public void HideSelection() {
        gameObject.SetActive(false);
        if (OnInactive != null) OnInactive(this);
    }



    Rect DefaultRect(Vector3 CenterPos, float width) {
        Vector2 ScreenPos = Camera.main.WorldToScreenPoint(CenterPos);
        Vector2 Offset = new Vector2(width, width);
        return new Rect(ScreenPos - Offset, Offset * 2);
    }

    Rect WorldToScreenBounds(Bounds Bounds) {
        float minX = Screen.width, maxX = 0, minY = Screen.height, maxY=0;

        for (int dx = 0; dx < 2; dx++) {
            for (int dy = 0; dy < 2; dy++) {
                for (int dz = 0; dz < 2; dz++) {
                    Vector2 ScreenPos = Camera.main.WorldToScreenPoint(Bounds.center + DirectedOffset(dx, dy, dz, Bounds.extents));
                    if (ScreenPos.x < minX) minX = ScreenPos.x;
                    if (ScreenPos.x > maxX) maxX = ScreenPos.x;
                    if (ScreenPos.y < minY) minY = ScreenPos.y;
                    if (ScreenPos.y > maxY) maxY = ScreenPos.y;
                }
            }
        }
        return new Rect(minX, minY, maxX - minX, maxY - minY);
    }

    Vector3 DirectedOffset(int dx, int dy, int dz, Vector3 extents) {
        return new Vector3(extents.x * (2 * dx - 1), extents.y * (2 * dy - 1), extents.z * (2 * dz - 1));
    }



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
