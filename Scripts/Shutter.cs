﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UIImage = UnityEngine.UI.Image;
using Act = System.Action;
using Synchro = AnimationSynchronized;

[RequireComponent(typeof(UIImage))]
public class Shutter : MonoBehaviour {    
    UIImage _myImg;
    UIImage MyImage {
        get {
            if (_myImg == null) {
                _myImg = GetComponent<UIImage>();
            }
            return _myImg;
        }
    }

    Synchro _Sync;
    Synchro Sync {
        get {
            if (_Sync == null) _Sync = FindObjectOfType<Synchro>();
            return _Sync;
        }
    }
    

    public bool startShut;

    public float Close(float Offset, float duration, Act Callback) {
        //if (MyImage.enabled == true) {
        //    if (Callback != null) Callback();
        //}
        //else {
        //Debug.Log("Suhutter close call: "+duration);
        //MyImage.enabled = true;
            return Sync.StartAnimation(Offset, duration, processClose, Callback);
        //}
    }


    void processClose(float pct) {
        Debug.Log("Closing Suhutter: "+pct);
        setElpha(pct);
    }
    void processOpen(float pct) {

        //Debug.Log("Opening Suhutter: "+pct);
        setElpha(1-pct);
    }

    void setElpha(float A) {
        MyImage.enabled = (A != 0);
        Color C = MyImage.color;
        C.a = A;
        MyImage.color = C;
    }


    public float Open(float Offset, float duration, Act Callback) {
        //Debug.Log("Suhutter open call: ");
        //if (MyImage.enabled == false) {
        ////    Debug.Log("Suhutter alreadt open");
        //    if (Callback != null) Callback();
        //    return Offset;
        //}
        //else {
            return Sync.StartAnimation(Offset, duration, processOpen, () => { MyImage.enabled = false; if (Callback != null) Callback(); });
        //}
    }

	// Use this for initialization
	void Awake () {
        setElpha(startShut ? 1 : 0);
        //MyImage.enabled = startShut;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
