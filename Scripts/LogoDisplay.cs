﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Act = System.Action;
using Synchro = AnimationSynchronized;


public class LogoDisplay : MonoBehaviour {

    Renderer _myRender;
    Renderer myRender {
        get {
            if (_myRender == null) _myRender = GetComponent<Renderer>();
            return _myRender;
        }
    }

    Synchro _Sync;
    Synchro Sync {
        get {
            if (_Sync == null) _Sync = FindObjectOfType<Synchro>();
            return _Sync;
        }
    }

    public float Appear(float Offset, float dur, Act Callback) {
        return Sync.StartAnimation(Offset, dur, ProcessAppear, Callback);
    }
    public float Hide(float Offset, float dur, Act Callback) {
        return Sync.StartAnimation(Offset, dur, ProcessHide, Callback);
    }
    public float Move(float Offset, float dur, Act Callback) {
        return Sync.StartAnimation(Offset, dur, ProcessMove, Callback);
    }

    public Color Revealed, Hidden;

    void ProcessAppear(float pct) {
        ProcessColor(pct);
    }
    void ProcessColor(float pct) {
        gameObject.SetActive(true);
            myRender.material.color = Color.Lerp(Hidden, Revealed, pct);
    }
    void ProcessHide(float pct) {
        ProcessColor(1 - pct);
    }

    public Vector3 Pos1;
    public Vector3 Pos2;

    void ProcessMove(float pct) {
        transform.localPosition = Vector3.Lerp(Pos1, Pos2, pct);
    }



    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
