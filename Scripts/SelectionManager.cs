﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionManager : MonoBehaviour {

    public static SelectionManager Main { get; private set; }

    Stack<Selection> Pool = new Stack<Selection>();
    public Selection Template;

    Selection getSelection () {
        if (Pool.Count>0) {
            return Pool.Pop();
        }
        else {
            Selection res = Instantiate(Template);
            res.OnInactive += Deactivated;
            res.transform.SetParent(transform, false);
            return res;
        }
    }

    void Deactivated(Selection Item) {
        //Pool.Push(Item);
        GameObject.Destroy(Item.gameObject );
    }

    Dictionary<GameObject, Selection> Cache = new Dictionary<GameObject, Selection>();

    public void ShowSelection(GameObject target, Color Color) {
        Selection sel;
        if (Cache.ContainsKey(target)==false) {
            sel = getSelection();
            Cache.Add(target, sel);
        }
        else {
            sel = Cache[target];
        }
        sel.ShowSelection(target, Color);
    }

    public void StopSelection(GameObject target) {
        if (Cache.ContainsKey(target) == false) return;
        Cache[target].HideSelection();
        Cache.Remove(target);
    }

	// Use this for initialization
	void Start () {
        Main = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
