﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarScatter : MonoBehaviour {

    public LayerMask HitLayer;
    public List<Material> Mats;
    public int StarCount;
    public List<TwinkyStar> Templates;
    
	// Use this for initialization
	void Start () {
        for (int i = 0; i < StarCount; i++) {
            Debug.Log(i);
            Ray posRay = Camera.main.ViewportPointToRay(new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f), 0));
            RaycastHit hitinfo;
            if (Physics.Raycast(posRay, out hitinfo, 9999, HitLayer)) {
                int Tind = Random.Range(0, Templates.Count);
                TwinkyStar tmp = Instantiate(Templates[Tind],hitinfo.point,Quaternion.identity,transform);
                tmp.SetMaterial(Mats[Random.Range(0, Mats.Count)]);
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
