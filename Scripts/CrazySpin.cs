﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrazySpin : MonoBehaviour {
    public float RSpeed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(Vector3.forward, RSpeed * Time.deltaTime);
	}
}
