﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarpEffect : MonoBehaviour {



    public void OpenWarp(float pct) {
        transform.localScale = Vector3.one * pct;
    }
    public void CloseWarp(float pct) {
        transform.localScale = Vector3.one * (1-pct);
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
