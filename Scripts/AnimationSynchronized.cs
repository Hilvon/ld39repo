﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationSynchronized : MonoBehaviour {
    public static AnimationSynchronized Main;

    public bool IsMain; 
	// Use this for initialization
	void Start () {
        if (IsMain) Main = this;
	}
	
	// Update is called once per frame
	void Update () {
        while (Timers1.Count>0) {
            Timer cur = Timers1.Pop();
            if (!cur.Estimate(Time.time))
                Timers2.Push(cur);
        }
        Stack<Timer> swap = Timers1;
        Timers1 = Timers2;
        Timers2 = swap;

		while (Animations.Count>0) {
            Animation cur = Animations.Pop();
            if (!cur.Run(Time.time))
                altStack.Push(cur);
        }

        Stack<Animation> tmp = Animations;
        Animations = altStack;
        altStack = tmp;

        if (OnAllComplete!=null && Animations.Count==0) {
            System.Action tmpCB = OnAllComplete;
            OnAllComplete = null;
            tmpCB();
        }
    }

    public void SetTimer(float Offset, System.Action Callback) {
        Timers1.Push(new Timer(Time.time + Offset, Callback));
    }

    public float StartAnimation(float offset, float Duration, System.Action<float> Callback, System.Action CompleteCallback = null) {
        Animations.Push(new Animation(Time.time + offset, Duration, Callback, CompleteCallback));
        return offset + Duration;
    }
    public void Wait(System.Action CallBack) {
        OnAllComplete += CallBack;
    }
    public void Rush() {
        while(Animations.Count>0) {
            Animations.Pop().Rush();
        }
    }
    System.Action OnAllComplete;

    Stack<Animation> Animations = new Stack<Animation>();
    Stack<Animation> altStack = new Stack<Animation>();

    Stack<Timer> Timers1 = new Stack<Timer>();
    Stack<Timer> Timers2 = new Stack<Timer>();

    class Timer {
        float AlertTime;
        System.Action Callback;
        public Timer(float Time, System.Action Callback) {
            AlertTime = Time;
            this.Callback = Callback;
        }
        public bool Estimate(float curTime) {
            bool res;
            if ((res = (curTime >= AlertTime)) && Callback != null) Callback();
            return res;
        }
    }


    class Animation {
        float startTime;
        float duration;
        System.Action<float> CallBack;
        System.Action CompleteCallBack;

        public bool Run( float curTime) {
            float CurProgress = (curTime - startTime) / duration;
            if (CurProgress < 0) return false;
            if (CurProgress >=1) {
                Rush();
                return true;
            }
            CallBack(CurProgress);
            return false;
        }
        public void Rush() {
            CallBack(1);
            if (CompleteCallBack != null) CompleteCallBack();
        }
        public Animation(float Start, float Dur, System.Action<float> CallBack, System.Action Complete) {
            startTime = Start;
            duration = Dur;
            this.CallBack = CallBack;
            CompleteCallBack = Complete;

            Debug.Log("Registering Animation: " + Dur+";" + Start);
        }
    }

}
