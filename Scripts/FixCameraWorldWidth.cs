﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixCameraWorldWidth : MonoBehaviour {

    public float RequiredWidth;

	// Use this for initialization
	void Awake () {
        AdjustWidth();
    }

    void AdjustWidth() {

        Camera.main.orthographicSize = Screen.height * RequiredWidth / Screen.width;

    }

	// Update is called once per frame
	void Update () {
        AdjustWidth();

    }
}
