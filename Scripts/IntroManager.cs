﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroManager : MonoBehaviour {

    public NarrativeRoll Exposition;
    public LogoDisplay Logo;



    public float fadeOut;
	// Use this for initialization
	void Start () {

        Logo.Move(0.5f, 4, null);
        Logo.Appear(0.5f, 0.25f, null);
        float LogoHide = Logo.Hide(4, 0.5f, null);

        float NarrationEnd = Exposition.RunAnimation(LogoHide-2, null);

        FindObjectOfType<Shutter>().Close(NarrationEnd - fadeOut, fadeOut, () => { UnityEngine.SceneManagement.SceneManager.LoadScene("Main"); });


        //Exposition.RunAnimation(0,() => {
        //    Debug.Log("Need to close shutter. Finding: "+ FindObjectOfType<Shutter>());
        //    FindObjectOfType<Shutter>().Close(0,fadeOut, () => {
        //        UnityEngine.SceneManagement.SceneManager.LoadScene("Main");
        //    });
        //});
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonUp(0)) {
            FindObjectOfType<AnimationSynchronized>().Rush();
        }
	}
}
