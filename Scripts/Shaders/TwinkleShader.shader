﻿Shader "Custom/TwinkleShader"
{
	Properties
	{
		[PerRendererData] _MainTex ("Texture", 2D) = "white" {}
		_C1 ("Color 1", Color) = (1,1,1,1)
		_C2 ("Color 2", Color) = (0,0,0,1)
		_Freq("Frequency", range(1,6)) = 1
	}
	SubShader
	{
		Tags { "RenderType"="Transparent" }
		LOD 100
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
				float4 tCol : COLOR;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _C1;
			float4 _C2;
			float _Freq;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.tCol = lerp(_C1,_C2,(1+sin(_Time.y*_Freq))/2);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				//return fixed4(i.uv.x,i.uv.y,i.uv.x,1);
				fixed dx = i.uv.x-0.5;
				fixed dy = i.uv.y-0.5;

				dx=1-lerp(-dx,dx,dx>0);
				dy=1-lerp(-dy,dy,dy>0);
				//return fixed4(dx,dx,dx,1);
				fixed dr = clamp(1-((1-dx)*(1-dx)+(1-dy)*(1-dy)),0,1);
				fixed dc = lerp(dx,dy,dy>dx);
				fixed a=dr*dr*dc*dc*dc*dc;
				//return fixed4(a,a,a,1);

				fixed4 col;// = tex2D(_MainTex, i.uv);
				//fixed a = col.a;
				col = i.tCol*a; //*
				col.a = a;
				

				// apply fog
				//UNITY_APPLY_FOG(i.fogCoord, col);
				//fixed4 col = _C1;
				return col;
			}
			ENDCG
		}
	}
}
