﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ErrDialog : MonoBehaviour {

    public static ErrDialog main;

    public Text _myText;
    Text myTest {
        get {
            if (_myText == null) {
                _myText = GetComponentInChildren<Text>();
            }
            return _myText;
        }
    }


    public void ShowError(string text) {
        myTest.text = text;
        SoundManager.Main.PlayErr();
        gameObject.SetActive(true);
    }

    public void CloseDialog() {
        gameObject.SetActive(false);
    }

    // Use this for initialization
    void Start() {
        main = this;
        gameObject.SetActive(false);
    }
}
