﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Linq;

public class GameManager : MonoBehaviour {
    public static GameManager Main;


    public Transform LeftSystem;
    public Transform RightSystem;

    public GameObject LeftLid;
    public GameObject RightLid;

    List<Ship> TransferList = new List<Ship>();
    public int MaxTransferVol = 2;


    public int portalCahrge;
    public int RequiredCivilianProtection;

    public int ChargeLeft { get; private set; }

    public bool TryTransfer(Ship Ship) {        
        if (ProjectedState.MovedShips() < MaxTransferVol) {
            if (MoveShip(Ship)) {
                return true;
            }
            else {
                if (Ship.isCivilian) {
                    ErrDialog.main.ShowError("We can't send Civilian ships to the other side. Without propper protection. We need at least "+RequiredCivilianProtection+" Military Power at destination");
                }
                else {
                    ErrDialog.main.ShowError("Sending this ship out will endanger civilian ships in this system. We need to maintain at least " + RequiredCivilianProtection + " Military Power here if we leave Civilians here");
                }
                return false;
            }
        }
        ErrDialog.main.ShowError("Maximum of " + MaxTransferVol + " ships can be transferred simultaniously");
        return false;
    }

    //public int AdjustedHerePower(int Adjust) {

    //}

    bool MoveShip(Ship S) {
        GameState tmp = ProjectedState.Change(S);
        if (tmp.IsValid(RequiredCivilianProtection)) {
            Debug.Log("NewSatte confirmed");
            ProjectedState = tmp;
            return true;
        }
        return false;
    }


    public bool CanCancel(Ship Ship) {
        if (ProjectedState.WillShipMove(Ship)) {
            Debug.Log("Trying to cancel");
            return MoveShip(Ship);
        }
        return false;
    }

    public void ConfirmTurn() {
        if (ProjectedState.MovedShips() > 0) {
            curState = ProjectedState;
            portalCahrge -= curState.Commit();
            ChargeCounter.ShowCount(portalCahrge);
            ProjectedState = curState.Change(null);
            if (!CheckEndGame()) {
                if (LeftLid.activeInHierarchy) {
                    LeftLid.SetActive(false);
                    RightLid.SetActive(true);
                }
                else {
                    LeftLid.SetActive(true);
                    RightLid.SetActive(false);
                }

            }
        }
        else {
            ErrDialog.main.ShowError("Please move at least one ship");
        }
    }


    //int HerePower {
    //    get {
    //        return IsLeftToRight ? ProjectedRight : ProjectedLeft;
    //    }
    //}
    //int TherePower {
    //    get {
    //        return IsLeftToRight ? ProjectedLeft : ProjectedRight;
    //    }
    //}


    //public int LeftPower { get; private set; }
    //public int RightPower { get; private set; }

    //public int Transferred { get; private set; }

    public bool IsLeftToRight { get; private set; }

    //public int ProjectedLeft { get { return LeftPower + (IsLeftToRight ? -1 : 1) * Transferred; } }
    //public int ProjectedRight { get { return RightPower + (IsLeftToRight ? 1 : -1) * Transferred; } }

    GameState curState;
    GameState ProjectedState;

    // Use this for initialization
    void Start () {
        FindObjectOfType<Shutter>().Open(0,0.5f, null);
        ChargeCounter.SetInitialCount(portalCahrge);
        curState = new GameState(LeftSystem.GetComponentsInChildren<Ship>().ToList());
        IsLeftToRight = true;
        curState.Commit();
        ProjectedState = curState.Change(null);
        Main = this;
    }

    bool CheckEndGame() {
        if (LeftSystem.GetComponentsInChildren<Ship>().Count() == 0) {
            Win();
            return true;
        }
        if (portalCahrge <=0) {
            Lose();
            return true;
        }
        return false;
    }

    public GameObject WinPanel;
    public GameObject LosePanel;
    public GameObject Menu;

    void Win() {
        Debug.Log("Win!");
        HideGame();
        WinPanel.SetActive(true);
        Menu.SetActive(true);
    }

    void Lose() {
        Debug.Log("Lose!");
        HideGame();
        LosePanel.SetActive(true);
        Menu.SetActive(true);
    }

    // Update is called once per frame
    void Update () {
		
	}

    void HideGame() {
        gameObject.SetActive(false);
        LeftLid.SetActive(false);
        RightLid.SetActive(false);
    }

    public void Retry() {
        FindObjectOfType<Shutter>().Close(0,0.5f,()=> { UnityEngine.SceneManagement.SceneManager.LoadScene("Main"); }); 
        
    }

    public void Exit() {
        Application.Quit();
    }

    public bool WillShipMove(Ship S) {
        return ProjectedState.WillShipMove(S);
    }

    public void ShipRepositioned(Ship S, bool isTranferred) {
        if (isTranferred) {
            S.transform.SetParent(RightSystem, true);
        }
        else {
            S.transform.SetParent(LeftSystem, true);
        }
    }



    class GameState {
        List<Ship> LeftShips = new List<Ship>();
        List<Ship> RightShips = new List<Ship>();

        public int MovedShips() {
            return LeftShips.Where((S) => { return S.isTransferred; }).Count() + RightShips.Where((S) => { return !S.isTransferred; }).Count();
        }

        public bool WillShipMove(Ship S) {
            if (S.isTransferred) return LeftShips.Contains(S);
            else return RightShips.Contains(S);
        }

        int getNuCivilians(List<Ship> List) {
            return List.Where((S) => { return S.isCivilian; }).Count();
        }

        public bool IsValid(int RequiredProtection) {
            return (LeftPower >= RequiredProtection || NumLeftCivilians == 0) && (RightPower >= RequiredProtection || NumRightCivilians == 0);
        }

        int NumLeftCivilians {
            get {
                return getNuCivilians(LeftShips);
            }
        }

        int NumRightCivilians {
            get {
                return getNuCivilians(RightShips);
            }
        }

        int GetTotalPower(List<Ship> List) {
            int res = 0;
            List.ForEach((S) => { res += S.MilitaryStr; });
            return res;

        }

        int LeftPower {
            get {
                return GetTotalPower(LeftShips);
            }
        }
        int RightPower {
            get {
                return GetTotalPower(RightShips);
            }
        }

        public GameState Change (Ship MovedShip) {
            GameState res = new GameState();

            res.LeftShips = LeftShips.ToList();
            res.RightShips = RightShips.ToList();
            if (MovedShip != null) {
                if (res.RightShips.Contains(MovedShip)) {
                    res.RightShips.Remove(MovedShip);
                    res.LeftShips.Add(MovedShip);
                }
                else {
                    res.LeftShips.Remove(MovedShip);
                    res.RightShips.Add(MovedShip);
                }
            }
            return res;
        }

        
        public int Commit() {
            int res = 0;
            foreach (Ship S in LeftShips) {
                if (S.isTransferred && S.TransferCost > res) res = S.TransferCost;
                S.isTransferred = false;
            }
            foreach (Ship S in RightShips) {
                if (!S.isTransferred && S.TransferCost > res) res = S.TransferCost;
                S.isTransferred = true;
            }
            return res;
        }

        GameState () { }

        public GameState(List<Ship> List) {
            foreach(Ship S in List) {
                if (S.isTransferred) RightShips.Add(S);
                else LeftShips.Add(S);
            }
        }

    }




}
