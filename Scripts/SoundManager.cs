﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {
    public static SoundManager Main;
    public AudioClip SelectSound;
    public AudioClip WarpSound;
    public AudioClip BadSound;

    Stack<AudioSource> Cache = new Stack<AudioSource>();

    AudioSource getSource() {
        if (Cache.Count > 0) {
            AudioSource res =  Cache.Pop();
            res.gameObject.SetActive(true);
            return res;
        }
        else {
            GameObject tmp = new GameObject();
            tmp.transform.SetParent(transform);
            return tmp.AddComponent<AudioSource>();
        }
    }

    
    

    public float PlayErr() {
        AudioSource AS = getSource();
        AS.clip = BadSound;
        AS.pitch = Random.Range(0.9f, 1.15f);
        AS.Play();

        AnimationSynchronized.Main.SetTimer(BadSound.length + 0.1f, () => {
            AS.gameObject.SetActive(false);
            Cache.Push(AS);
        });

        return BadSound.length;        
    }

    void Awake() {
        if (Main==null) {
            Main = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }

    public float PlayWarp() {
        AudioSource AS = getSource();
        AS.clip = WarpSound;
        AS.pitch = Random.Range(0.9f, 1.15f);
        AS.Play();
        AnimationSynchronized.Main.SetTimer(WarpSound.length + 0.1f, () => {
            AS.gameObject.SetActive(false);
            Cache.Push(AS);
        });
        return WarpSound.length;
    }

    public void PlaySelect() {

        AudioSource AS = getSource();

        AS.clip = SelectSound;
        AS.Play();

        AnimationSynchronized.Main.SetTimer(SelectSound.length+0.1f, () => {
            AS.gameObject.SetActive(false);
            Cache.Push(AS);
        });

        //SelectSound.
    }

    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
