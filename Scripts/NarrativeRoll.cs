﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NarrativeRoll : MonoBehaviour {

    public RectTransform Controlled;
    public float StartHt;
    public float EndHt;

    public float duration;

    void Animate(float pct) {

        Vector2 curPos = Controlled.anchoredPosition;
        curPos.y = Mathf.Lerp(StartHt, EndHt, pct);
        Controlled.anchoredPosition = curPos;
        if (pct >= CritPct && callBack != null) {
            callBack();
            callBack = null;
        }
    }

    float CritPct;
    System.Action callBack;

    public float RunAnimation(float offset, System.Action CallBack) {
        return GameObject.FindObjectOfType<AnimationSynchronized>().StartAnimation(offset, duration, Animate, CallBack);
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
