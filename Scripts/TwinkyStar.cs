﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwinkyStar : MonoBehaviour {

    Renderer _myR;
    Renderer myR {
        get {
            if (_myR == null) _myR = GetComponent<Renderer>();
            return _myR;
        }
    }

    public Color C1, C2;
    public float CFreq;
    public float S1;
    public float S2;
    public float SFreq;
    public float RSpeed;

    public void SetMaterial(Material M) {
        myR.sharedMaterial = M;
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.localScale = Vector3.one * Mathf.Lerp(S1, S2, (1 + Mathf.Sin(Time.time / SFreq))/2);
        transform.Rotate(Vector3.forward, RSpeed*Time.deltaTime);
        //myR.ma
	}
}
