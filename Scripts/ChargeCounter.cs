﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ChargeCounter : MonoBehaviour {

    static float getCurPct(int Cnt) {
        return Cnt / InitialCnt;
    }

    public static void SetInitialCount(int Cnt) {
        InitialCnt = Cnt;
        LastCount = Cnt;
        if (_DisplayNumber != null) _DisplayNumber(Cnt, 1);
    }

    public static void ShowCount(int Cnt) {
        LastCount = Cnt;
        if (_DisplayNumber != null) _DisplayNumber(Cnt, Cnt/InitialCnt);
    }
    static System.Action<int, float> _DisplayNumber;
    static event System.Action<int, float> DisplayNumber { add {
            if (value != null) {
                _DisplayNumber += value;
                value(LastCount,LastCount / InitialCnt);
            }
        }
        remove {
            _DisplayNumber -= value;
        }
    } 
    



    public Color InitialColor;
    public Color DepletedColor;

    public Text Counter;
    public Text Label;

    static float InitialCnt;
    static int LastCount;

    Color getColor(float pct) {
        return Color.Lerp(DepletedColor, InitialColor, pct);
    }

    private void OnDestroy() {
        DisplayNumber -= ShowState;
    }

    void ShowState (int Count, float pct) {
        Counter.text = "" + Count;
        Counter.color = Label.color = getColor(pct);
    }

    

    


	// Use this for initialization
	void Start () {
        DisplayNumber += ShowState;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
